 <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website <?=date('Y');?></p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- jQuery -->
    <script src="<?=ASSETS?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=ASSETS?>js/bootstrap.min.js"></script>

</body>

</html>
