<?php 
   session_start();
    // Create connection
    $conn = mysqli_connect(DBSERVER, DBUSER, DBPASSW, DBNAME);
    // Check connection
        if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
        } 
     
    if(isset($_POST['signin']) && $_POST['signin']=='SIGNIN')
    {
        
        $uname=trim($_POST['user']);
        $upass=trim($_POST['password']);
        $upass=md5($upass);                                         //hashing password
        $sql = "SELECT * FROM tbl_user WHERE username='$uname' AND password='$upass' AND status=1";
        $result = mysqli_query($conn, $sql);
        //checking id data row exist
        if (mysqli_num_rows($result) > 0) 
        {
        $row = mysqli_fetch_assoc($result);
        //login success and redirect
            $_SESSION['user_login']=TRUE;
            $_SESSION['uid']=$row["id"];
            $_SESSION['user']=$row["username"];
      } 
        else 
        {
            //login fail, show error
            $msg='Username or Password Invalid, Try Again';
            $stat=0;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="personal Blog, Tech-info">
    <meta name="author" content="Satish Maharjan">

    <title>Blog Post - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=ASSETS?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=ASSETS?>css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      .carousel-inner > .item > img,
      .carousel-inner > .item > a > img {
          width: 70%;
          margin: auto;
      }
    </style>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Site Name</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=SITEURL?>">Home</a>
                    </li>
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                    <?php if(isset($_SESSION['user_login']) && $_SESSION['user_login']===TRUE){?>
                        <li>
                        <a href="addArticle.php">Add Article</a>
                        </li>
                        <li>
                        <a href="userArticle.php">My Article</a>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(isset($_SESSION['user_login']) && $_SESSION['user_login']===TRUE){?>
                    <div class="navbar-form navbar-right greeting">
                        Hello, <?=$_SESSION['user']?>  &nbsp;<a href="logout.php" class="btn btn-info">logout</a>
                    </div>
                <?php }else{ ?>
                    <form class="navbar-form navbar-right" method="post">
                        <?php if(isset($msg) && $stat===0){ ?>
                        <span class="label label-danger">Invalid, Try Again.</span>
                        <?php } ?>
                        <div class="form-group">
                          <input placeholder="Username" class="form-control" name="user" required type="text">
                        </div>
                        <div class="form-group">
                          <input placeholder="Password" class="form-control" name="password" required type="password">
                        </div>
                        <button type="submit" name="signin" value="SIGNIN" class="btn btn-success">Sign in</button>
                  </form>
                <?php } ?>
            </div>
            
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
