<?php
//Defining Server Constants
	define("DBSERVER","localhost",true);
	define("DBUSER","root");
	define("DBPASSW","toor");
	define("DBNAME","demo_db",true);
	define('SYSTEM_EMAIL',"info@apex.edu.np");
	define("SITEURL", "http://localhost/demo/",true);
	define("SITEROOTDOC",$_SERVER['DOCUMENT_ROOT']."/demo/");
        
	define("FILEPATH",SITEROOTDOC."includes/");
	define("ADMINPATH",SITEURL."cms/");
	define("IMAGEPATH",SITEROOTDOC."uploads/");
  define("SAVEADDS",SITEROOTDOC."infosheet/");
        
	define("GALLERYPATH",SITEROOTDOC."gallery/");
	define("ASSETS",SITEURL."assets/");
	define("VISIBLEIMAGE",SITEURL."uploads/");
	date_default_timezone_set("Asia/Bangkok");
?>