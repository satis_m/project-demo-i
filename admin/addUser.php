<?php require_once('../includes/configs.php');
  session_start();

  //checking session for logged in admin 
  if(!isset($_SESSION['admin_login']) || $_SESSION['admin_login']!=TRUE)
  {
    header("Location: ".SITEURL."admin");
    die();
  }
  $css=array();     // adding css source
  $jquery=array( 
      ASSETS."admin/js/parsley/parsley.min.js",
      ASSETS."admin/js/parsley/parsley.extend.js"
  );  // adding jquery library source
  $script=array();  // decleation jquery function
  $jscript='';      // plain javascript 
?>
<?php require_once('common/header.php');?>
<?php 
  if(isset($_POST['add']) && $_POST['add']=='ADD')
  {
    //insert
    $uname=trim($_POST['uname']);
    $upass=trim($_POST['upass']);
    $upass=md5($upass);
    $fname=trim($_POST['fname']);
    $lname=trim($_POST['lname']);
    $email=trim($_POST['email']);
    $phone=trim($_POST['phone']);
    $gender=trim($_POST['gender']);
    $sql = "INSERT INTO tbl_user ( fname, lname, gender, phone, username, password, email, status)
    VALUES ('$fname', '$lname', '$gender', '$phone', '$uname', '$upass', '$email', 1)";

    if (mysqli_query($conn, $sql)) {
        $msg="New user created successfully";
        $stat=1;
    } else {
        $msg="Error: " . $sql . "<br>" . mysqli_error($conn);
        $stat=0;
    }
  }
?>
<?php require_once('common/sidebar.php');?>
  <!-- /.aside -->
  <section id="content">
      <section class="vbox">
          <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                  <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                  <li><a href="#">User</a></li>
                  <li class="active">Add User</li>
              </ul>
              <div class="m-b-md">
                  <h3 class="m-b-none">Add User</h3> </div>
              <div class="row">
                  <div class="col-sm-12">
                      <?php if(isset($msg) && $stat===1){ ?>
                        <div class='alert alert-success'> 
                        <?=$msg?>
                        </div>
                      <?php } else if(isset($msg) && $stat===0){ ?>
                        <div class='alert alert-danger'>
                          <?=$msg?>
                        </div>
                      <?php } ?>
                      <form class="form-horizontal" method="post" data-validate="parsley">
                          <section class="panel panel-default">
                              <header class="panel-heading"> <strong>Information</strong> </header>
                              <div class="panel-body">
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">User Name:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="uname" class="form-control" placeholder="First Name"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Password:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="upass" class="form-control" placeholder="First Name"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">First Name:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="fname" class="form-control" placeholder="First Name"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Last Name:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="lname" class="form-control" placeholder="Last Name"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Gender:</label>
                                      <div class="col-sm-3">
                                          <select data-required="true" name="gender" class="form-control m-b">
                                              <option value="">Please choose</option>
                                              <option value="foo">male</option>
                                              <option value="female">female</option>
                                              <option value="other">other</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Email:</label>
                                      <div class="col-sm-3">
                                          <input type="email" data-required="true" name="email" class="form-control" placeholder="Email"> </div>
                                  </div>
                                  <!-- <div class="line line-dashed line-lg pull-in"></div> -->
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Phone:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="phone" class="form-control" data-type="phone"  placeholder="Phone"> </div>
                                  </div>
                              </div>
                              <footer class="panel-footer text-left bg-light lter">
                                  <button type="submit" name="add" value="ADD" class="btn btn-success btn-s-xs">Add</button>
                                  <button type="reset" class="btn btn-danger btn-s-xs">Cancel</button>
                              </footer>
                          </section>
                      </form>
                  </div>
              </div>
          </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
  </section>
<?php require_once('common/footer.php');?>
               