<?php require_once('../includes/configs.php'); ?>
<?php 
	session_start();
	if(isset($_SESSION['admin_login']) && $_SESSION['admin_login']===TRUE)
	{
		header("Location: ".SITEURL."admin/manageArticle.php");
		die();
	}
	if(isset($_POST['login']) && $_POST['login']=='LOGIN')
	{
		// Create connection
		$conn = mysqli_connect(DBSERVER, DBUSER, DBPASSW, DBNAME);
		// Check connection
			if (!$conn) {
		    die("Connection failed: " . mysqli_connect_error());
			}
		$uname=trim($_POST['uname']);
		$upass=trim($_POST['upass']);
		$upass=md5($upass);											//hashing password
		$sql = "SELECT * FROM tbl_admin WHERE username='$uname' AND password='$upass' AND status=1";
		$result = mysqli_query($conn, $sql);
		//checking id data row exist
		if (mysqli_num_rows($result) > 0) 
		{
	   	$row = mysqli_fetch_assoc($result);
	    //login success and redirect
			$_SESSION['admin_login']=TRUE;
			$_SESSION['uid']=$row["id"];
			$_SESSION['username']=$row["username"];
	  	header("Location: ".SITEURL."admin/manageArticle.php");
			die();
	  } 
		else 
		{
			//login fail, show error
	   	$msg='Username or Password Invalid, Try Again';
	   	$stat=0;
		}
	}
?>
<!DOCTYPE html>
<html lang="en" class="bg-dark">

<head>
    <meta charset="utf-8" />
    <title>Notebook | Web Application</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="author" content="Satish Maharjan">
    <link rel="stylesheet" href="<?=ASSETS?>admin/css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="<?=ASSETS?>admin/css/font.css" type="text/css" cache="false" />
    <!--[if lt IE 9]> <script src="<?=ASSETS?>admin/js/ie/html5shiv.js" cache="false"></script> <script src="<?=ASSETS?>admin/js/ie/respond.min.js" cache="false"></script> <script src="<?=ASSETS?>admin/js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>
<body>
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
        <div class="container aside-xxl"> <a class="navbar-brand block" href="index.html">Notebook</a>
            <section class="panel panel-default bg-white m-t-lg">
                <header class="panel-heading text-center"> <strong>Sign in</strong> </header>
                <form action="" method="post" class="panel-body wrapper-lg">
		              <?php if(isset($msg) && $stat==0){?> 
		                <div class="alert alert-danger"> 
		                	<?=$msg?>
		                </div>
		              <?php } ?>
                  <div class="form-group">
                      <label class="control-label">Username</label>
                      <input type="text" placeholder="Username" name="uname" class="form-control input-lg"> 
                  </div>
                  <div class="form-group">
                      <label class="control-label">Password</label>
                      <input type="password" id="inputPassword" name="upass" placeholder="Password" class="form-control input-lg"> 
                  </div>
                  <a href="#" class="pull-right m-t-xs"><small>Forgot password?</small></a>
                  <button type="submit" name='login' value="LOGIN" class="btn btn-primary">Sign in</button>
                </form>
            </section>
        </div>
    </section>
    <!-- footer -->
    <footer id="footer">
        <div class="text-center padder">
            <p> <small>Web app framework base on Bootstrap<br>&copy; 2013</small> </p>
        </div>
    </footer>
    <!-- / footer -->
    <script src="<?=ASSETS?>admin/js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
</body>

</html>

               