<?php require_once('includes/configs.php'); ?>
<?php require_once('common/header.php'); ?>
<?php
$id=trim($_GET['id']);
    if(isset($_SESSION['admin_login']) && $_SESSION['admin_login']===TRUE && isset($_GET['adview']) && $_GET['adview']==1)
    {
        $sql = "SELECT * FROM tbl_article WHERE id=$id ";
    }
    else
    {
        $sql = "SELECT * FROM tbl_article WHERE status=1 AND id=$id ";
    }
    $result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $row = mysqli_fetch_assoc($result)

?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-12">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?=$row['title']?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#"><?=$row['uid']?></a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?=date("F t, Y \a\\t g:i a", strtotime($row['added_on']));?></p>

                <hr>

                <!-- Preview Image -->
                <img class="img-thumbnail" src="<?=$row['image']?>" alt="">

                <hr>

                <!-- Post Content -->
                <?=$row['content']?>
                
            </div>
        </div>
        <!-- /.row -->

        <hr>

       
    <!-- /.container -->
<?php }else{ ?>
<div class="jumbotron">
    <div class="container">
        <h1>Sorry, Article not found. </h1>
      </div>
</div>
<?php } require_once('common/footer.php'); ?>