 <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="<?=ASSETS?>admin/js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="<?=ASSETS?>admin/js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/charts/flot/demo.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/calendar/demo.js" cache="false"></script>
    <script src="<?=ASSETS?>admin/js/sortable/jquery.sortable.js" cache="false"></script>

    <?php 
        if(isset($jquery) && !empty($jquery))
        {
            foreach($jquery as $src)
            {
                echo ' <script src="'.$src.'"cache="false"></script>'."\n";
            }
        }
    ?>
    
    <script>
        (function($){
            <?php
            if(isset($script) && !empty($script))
            {
                foreach($script as $jfunction)
                {
                    echo $jfunction;
                }
            }
            ?>
        })(jQuery);
    </script>
    <script>
        <?php
            if(isset($jscript) && $jscript!='')
            {
                echo $jscript;
            }
        ?>
    </script>
</body>

</html>
<?php mysqli_close($conn); ?>