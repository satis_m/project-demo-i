<?php require_once('../includes/configs.php');
  session_start();

  //checking session for logged in admin 
  if(!isset($_SESSION['admin_login']) || $_SESSION['admin_login']!=TRUE)
  {
    header("Location: ".SITEURL."admin");
    die();
  }
  $css=array();     // adding css source
  $jquery=array( 
      ASSETS."admin/js/parsley/parsley.min.js",
      ASSETS."admin/js/parsley/parsley.extend.js"
  );  // adding jquery library source
  $script=array();  // decleation jquery function
  $jscript='';      // plain javascript 
?>
<?php require_once('common/header.php');?>
<?php require_once('common/sidebar.php');?>
  <?php 
  $id=$_GET['id'];
  if(isset($_POST['update']) && $_POST['update']=='Update')
  {
    //insert
    $fname=trim($_POST['fname']);
    $lname=trim($_POST['lname']);
    $email=trim($_POST['email']);
    $phone=trim($_POST['phone']);
    $gender=trim($_POST['gender']);
    $status=trim($_POST['status']);
    if(isset($_POST['change_pass']) && $_POST['change_pass']==1){
      $upass=trim($_POST['upass']);
      $upass=md5($upass);
      $sql = " UPDATE tbl_user SET fname='$fname', lname='$lname', gender='$gender', phone='$phone', password='$upass', email='$email', status='$status'  WHERE id=$id";
    }
    else
    {
      $sql = " UPDATE tbl_user SET fname='$fname', lname='$lname', gender='$gender', phone='$phone', email='$email', status='$status'  WHERE id=$id";
    }

    if (mysqli_query($conn, $sql)) {
        $msg="user Info Updated successfully";
        $stat=1;
    } else {
        $msg="Error: " . $sql . "<br>" . mysqli_error($conn);
        $stat=0;
    }
  }

  $sql = "SELECT * FROM tbl_user WHERE id=$id";
  $result = mysqli_query($conn, $sql);
  $row = mysqli_fetch_assoc($result)
  ?>
  <!-- /.aside -->
  <section id="content">
      <section class="vbox">
          <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                  <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                  <li><a href="#">User</a></li>
                  <li class="active">Add User</li>
              </ul>
              <div class="m-b-md">
                  <h3 class="m-b-none">Add User</h3> </div>
              <div class="row">
                  <div class="col-sm-12">
                  <div class="col-sm-12">
                      <?php if(isset($msg) && $stat===1){ ?>
                        <div class='alert alert-success'> 
                        <?=$msg?>
                        </div>
                      <?php } else if(isset($msg) && $stat===0){ ?>
                        <div class='alert alert-danger'>
                          <?=$msg?>
                        </div>
                      <?php } ?>
                      <form class="form-horizontal" method="post" data-validate="parsley">
                          <section class="panel panel-default">
                              <header class="panel-heading"> <strong>Information</strong> </header>
                              <div class="panel-body">
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">User Name:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="uname" disabled class="form-control" value="<?=$row['username']?>" placeholder="First Name"> </div>
                                  </div>
                                  <div class="form-group"> 
                                    <label class="col-sm-3 control-label">Password:</label> 
                                    <div class="col-sm-3"> 
                                      <input type="text" class="form-control" name="upass" placeholder="New Password">  
                                      <label class="checkbox"> 
                                        <input type="checkbox" name="change_pass" value="1"> Change Password
                                      </label> 
                                    </div> 
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">First Name:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="fname" class="form-control" value="<?=$row['fname']?>" placeholder="First Name"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Last Name:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="lname" class="form-control" value="<?=$row['lname']?>" placeholder="Last Name"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Gender:</label>
                                      <div class="col-sm-3">
                                          <select data-required="true" name="gender" class="form-control m-b">
                                              <option value="">Please choose</option>
                                              <option <?=($row['gender']=='male' ? 'selected': '' )?> value="male">male</option>
                                              <option <?=($row['gender']=='female' ? 'selected': '' )?> value="female">female</option>
                                              <option <?=($row['gender']=='other' ? 'selected': '' )?> value="other">other</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Email:</label>
                                      <div class="col-sm-3">
                                          <input type="email" data-required="true" name="email" class="form-control" value="<?=$row['email']?>"  placeholder="Email"> </div>
                                  </div>
                                  <!-- <div class="line line-dashed line-lg pull-in"></div> -->
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Phone:</label>
                                      <div class="col-sm-3">
                                          <input type="text" data-required="true" name="phone" class="form-control" value="<?=$row['phone']?>" data-type="phone"  placeholder="Phone"> </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-3 control-label">Status:</label>
                                      <div class="col-sm-3">
                                          <select data-required="true" name="status" class="form-control m-b">
                                              <option value="">Please choose</option>
                                              <option <?=($row['status']=='1' ? 'selected': '' )?> value="1">Active</option>
                                              <option <?=($row['status']=='0' ? 'selected': '' )?> value="0">Blocked</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                              <footer class="panel-footer text-left bg-light lter">
                                  <button type="submit" name="update" value="Update" class="btn btn-success btn-s-xs">Update</button>
                                  <button type="reset" class="btn btn-danger btn-s-xs">Cancel</button>
                              </footer>
                          </section>
                      </form>
                  </div>
              </div>
          </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
  </section>
<?php require_once('common/footer.php');?>
               