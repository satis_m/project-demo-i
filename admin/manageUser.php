<?php require_once('../includes/configs.php');
  session_start();

  //checking session for logged in admin 
  if(!isset($_SESSION['admin_login']) || $_SESSION['admin_login']!=TRUE)
  {
    header("Location: ".SITEURL."admin");
    die();
  }
  $css=array();     // adding css source
  $jquery=array( 
      ASSETS."admin/js/parsley/parsley.min.js",
      ASSETS."admin/js/parsley/parsley.extend.js"
  );  // adding jquery library source
  $script=array();  // decleation jquery function
  $jscript='';      // plain javascript 
?>
<?php require_once('common/header.php');?>
<?php 
  if(isset($_POST['add']) && $_POST['add']=='ADD')
  {
    //insert
    $uname=trim($_POST['uname']);
    $upass=trim($_POST['upass']);
    $fname=trim($_POST['fname']);
    $lname=trim($_POST['lname']);
    $email=trim($_POST['email']);
    $phone=trim($_POST['phone']);
    $gender=trim($_POST['gender']);
    $sql = "INSERT INTO tbl_user ( fname, lname, gender, phone, username, password, email, status)
    VALUES ('$fname', '$lname', '$gender', '$phone', '$uname', '$upass', '$email', 1)";

    if (mysqli_query($conn, $sql)) {
        $msg="New user created successfully";
        $stat=1;
    } else {
        $msg="Error: " . $sql . "<br>" . mysqli_error($conn);
        $stat=0;
    }
  }
?>
<?php require_once('common/sidebar.php');?>
  <!-- /.aside -->
  <section id="content">
      <section class="vbox">
          <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                  <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                  <li><a href="#">User</a></li>
                  <li class="active">Manage User</li>
              </ul>
              <div class="m-b-md">
                  <h3 class="m-b-none">Manage User</h3> </div>
              <div class="row">
                  <div class="col-sm-12">
                  <?php if(isset($_GET['delete']) && $_GET['delete']!='')
                  { 
                    if($_GET['delete']==1)
                    { ?>
                          <div class='alert alert-success'> 
                            User And Article Deleted Succesfully
                          </div>
                        <?php } else if($_GET['delete']==0){ ?>
                          <div class='alert alert-danger'>
                            Error Occured In database, Tray Again.
                          </div>
                      <?php 
                    } ?>
                    <?php 
                  } ?>
                    <section class="panel panel-default">
                      <header class="panel-heading"> <strong>User List</strong> </header>
                      <div class="panel-body">
                          <table class='table table-bordered table-striped'>
                            <thead>
                              <tr>
                              <th>sn</th>
                              <th>username</th>
                              <th>fname</th>
                              <th>lname</th>
                              <th>email</th>
                              <th>status</th>
                              <th>action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $sql = "SELECT * FROM tbl_user ORDER BY fname ASC";
                                $result = mysqli_query($conn, $sql);

                                if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                  $i=1;
                                while($row = mysqli_fetch_assoc($result)) { ?>
                                  <tr>
                                    <td><?=$i++;?></td>
                                    <td><?=$row['username']?></td>
                                    <td><?=$row['fname']?></td>
                                    <td><?=$row['lname']?></td>
                                    <td><?=$row['email']?></td>
                                    <td><?php if($row['status']==1){ ?><span class='label label-success'>Active </span><?php } else{?><span class='label label-danger'>Blocked </span> <?php } ?></td>
                                    <td>
                                      <a href="editUser.php?id=<?=$row['id']?>" class="btn btn-info"> Edit</a>
                                      <a href="deleteUser.php?id=<?=$row['username']?>" class="btn btn-danger" onclick="return confirm('are you sure?')"> Delete</a>
                                    </td>
                                  </tr>
                              <?php } }else{?>
                                <tr> <td colspan="7" class="text-center">No Records Available</td> </tr>
                                <?php } ?>
                            </tbody>
                          </table>
                      </div>
                    </section>
                  </div>
              </div>
          </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
  </section>
<?php require_once('common/footer.php');?>
               