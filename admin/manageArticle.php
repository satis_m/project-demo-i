<?php require_once('../includes/configs.php');
  session_start();

  //checking session for logged in admin 
  if(!isset($_SESSION['admin_login']) || $_SESSION['admin_login']!=TRUE)
  {
    header("Location: ".SITEURL."admin");
    die();
  }
  $css=array();     // adding css source
  $jquery=array( 
      ASSETS."admin/js/parsley/parsley.min.js",
      ASSETS."admin/js/parsley/parsley.extend.js"
  );  // adding jquery library source
  $script=array();  // decleation jquery function
  $jscript='';      // plain javascript 
?>
<?php require_once('common/header.php');?>
<?php 
  if(isset($_POST['add']) && $_POST['add']=='ADD')
  {
    //insert
    $uname=trim($_POST['uname']);
    $upass=trim($_POST['upass']);
    $fname=trim($_POST['fname']);
    $lname=trim($_POST['lname']);
    $email=trim($_POST['email']);
    $phone=trim($_POST['phone']);
    $gender=trim($_POST['gender']);
    $sql = "INSERT INTO tbl_user ( fname, lname, gender, phone, username, password, email, status)
    VALUES ('$fname', '$lname', '$gender', '$phone', '$uname', '$upass', '$email', 1)";

    if (mysqli_query($conn, $sql)) {
        $msg="New user created successfully";
        $stat=1;
    } else {
        $msg="Error: " . $sql . "<br>" . mysqli_error($conn);
        $stat=0;
    }
  }
?>
<?php require_once('common/sidebar.php');?>
  <!-- /.aside -->
  <section id="content">
      <section class="vbox">
          <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                  <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                  <li class="active">Manage Article</li>
              </ul>
              <div class="m-b-md">
                  <h3 class="m-b-none">Manage Article</h3> </div>
              <div class="row">
                  <div class="col-sm-12">
                    <section class="panel panel-default">
                      <header class="panel-heading"> <strong>Article List</strong> </header>
                      <?php if(isset($_GET['update']) && $_GET['update']!=''){ ?>
                        <?php if($_GET['update']==1){ ?>
                          <div class='alert alert-success'> 
                            Article Updated Succesfully
                          </div>
                        <?php } else if($_GET['update']==0){ ?>
                          <div class='alert alert-danger'>
                            Error Occured In database, Tray Again.
                          </div>
                        <?php } ?>
                      <?php } ?>
                      <div class="panel-body">
                          <table class='table table-bordered table-striped'>
                            <thead>
                              <tr>
                              <th>sn</th>
                              <th>Title</th>
                              <th>Content</th>
                              <th>Image</th>
                              <th>uid</th>
                              <th>Published On</th>
                              <th>Status</th>
                              <th width="150px">action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                $sql = "SELECT * FROM tbl_article ORDER BY id DESC";
                                $result = mysqli_query($conn, $sql);

                                if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                  $i=1;
                                while($row = mysqli_fetch_assoc($result)) { ?>
                                  <tr>
                                    <td><?=$i++;?></td>
                                    <td><?=$row['title']?></td>
                                    <?php                                           
                                          $desc = $row['content'];
                                          $stringCut = substr($desc,0,250);
                                          $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                                    ?>
                                    <td><?=$string?></td>
                                    <td><div style='width:200px;' ><img src="<?=SITEURL?><?=$row['image']?>" width="100%" /></div></td>
                                    <td><?=$row['uid']?></td>
                                    <td><?=date("F t, Y \a\\t g:i a", strtotime($row['added_on']));?></td>
                                    <td><?php if($row['status']==1){ ?><span class='label label-success'>Published </span><?php } else{?><span class='label label-danger'>Blocked </span> <?php } ?></td>
                                    <td>
                                      <a href="<?=SITEURL?>viewDetail.php?id=<?=$row['id']?>&adview=1" class="btn btn-sm btn-info" > View</a>
                                      <?php if($row['status']==1){ ?>
                                        <a href="articleAction.php?id=<?=$row['id']?>&stat=block" class="btn btn-sm btn-danger" onclick="return confirm('are you sure?')"> Block</a>
                                      <?php } else{?>
                                        <a href="articleAction.php?id=<?=$row['id']?>&stat=publish" class="btn btn-sm btn-success" onclick="return confirm('are you sure?')"> Publish</a>
                                      <?php } ?>
                                      
                                    </td>
                                  </tr>
                              <?php } }else{?>
                                <tr> <td colspan="7" class="text-center">No Records Available</td> </tr>
                                <?php } ?>
                            </tbody>
                          </table>
                      </div>
                    </section>
                  </div>
              </div>
          </section>
      </section>
      <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
  </section>
<?php require_once('common/footer.php');?>
               