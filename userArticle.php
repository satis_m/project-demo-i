<?php require_once('includes/configs.php'); ?>
<?php require_once('common/header.php'); ?>
<?php
    $uid=$_SESSION['user'];
  ?>
<div class="jumbotron">
	<div class="container">
	  	<h1>My Article</h1>
	  </div>
</div>
<div class="container">

     <div class="row">

            <!-- Blog Post Content Column -->
           <div class="col-xs-12 col-sm-12">
              <p class="pull-right visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
              </p>
              <div class="row">
                <?php
                $sql = "SELECT * FROM tbl_article WHERE uid='$uid' AND status=1  ORDER BY id DESC";
                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                  $i=1;
                    while($row = mysqli_fetch_assoc($result)) {
                        ?>
                         <div class="col-xs-6 col-lg-6 ">
                          <h2><?=$row['title']?></h2>
                          <?php                                           
                                $desc = $row['content'];
                                $stringCut = substr($desc,0,250);
                                $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                          ?>
                          <p><?=strip_tags($string);?> </p>
                          <p><a class="btn btn-default" href="viewDetail.php?id=<?=$row['id']?>" role="button">View details »</a></p>
                        </div>
                        <?php if($i%2==0){?><div class="clearfix"></div><?php } $i++;?>
                        <?php
                    }
                } else { ?>
                    <div class="col-xs-12">
                          <br/><div class="alert alert-info text-center">No Article Available.</div>
                        </div>
                    <?php
                }?>
              </div><!--/row-->
            </div>
        </div>

<?php require_once('common/footer.php'); ?>
<script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
