<?php require_once('../includes/configs.php');
  session_start();

  //checking session for logged in admin 
  if(!isset($_SESSION['admin_login']) || $_SESSION['admin_login']!=TRUE)
  {
    header("Location: ".SITEURL."admin");
    die();
  }
  $conn = mysqli_connect(DBSERVER, DBUSER, DBPASSW, DBNAME);
    // Check connection
      if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
      }
  if(isset($_GET['id']) && $_GET['stat']!='')
  {
    $id=$_GET['id'];
    if($_GET['stat']=='block')
    {
      $stat=0;
    }
    else if($_GET['stat']=='publish')
    {
       $stat=1;
    }
    $sql = "UPDATE tbl_article SET status=$stat WHERE id=$id";
    if (mysqli_query($conn, $sql)) {
      header("Location: ".SITEURL."admin/manageArticle.php?update=1");
      die();
    } else {
      header("Location: ".SITEURL."admin/manageArticle.php?update=0");
      die();
    }
  }
?>
